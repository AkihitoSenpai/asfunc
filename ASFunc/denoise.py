############### DENOISERS ################

from typing import Dict, List, Optional, Union
import vapoursynth as vs
import vsutil
import EoEfunc as eoe
from vsdenoise import CCDPoints, ccd

core = vs.core

def BM3D(
    src: vs.VideoNode,
    sigma: Union[float, List[float], List[List[float]]] = 3,
    radius: Union[Optional[int], List[Optional[int]]] = 1,
    profile: Union[Optional[str], List[Optional[str]]] = "lc",
    refine: int = 1,
    pre: Optional[vs.VideoNode] = None,
    ref: Optional[vs.VideoNode] = None,
    matrix: Optional[str] = None,
    full_range: Optional[bool] = None,
    skip_basic: bool = False,
    CUDA: Optional[Union[bool, List[bool]]] = None,
    chroma: bool = True,
    device_id: int = 0,
    fast: bool = True,
    **kwargs,
):
    from . import format
    from vsutil.clips import get_y, split, join
    import sys

    input_original = src

    def is_gray(clip: vs.VideoNode) -> bool:
        return clip.format.color_family == vs.GRAY

    if CUDA is None:
        CUDA = [False, False]
        if hasattr(core, "bm3dcuda"):
            CUDA = [True, True]
    elif isinstance(CUDA, bool):
        CUDA = [CUDA, CUDA]

    if not isinstance(sigma, list):
        sigma = [sigma]
    if not all(isinstance(elem, list) for elem in sigma):
        sigma = [sigma, sigma]
    sigma: List[List[float]] = [(s + [s[-1]] * 3)[:3] for s in sigma]
    for i in [0, 1]:
        # multiply luma sigmas by 0.8, and chroma by 0.95, if cuda
        # this seemed to give closer results to fully non cuda.
        if CUDA[i]:
            sigma[i] = [sigma[i][0] * 0.8] + [s * 0.95 for s in sigma[i][1:]]

    if not isinstance(profile, list):
        profile = [profile, profile]
    elif profile[1] is None:
        profile = [profile[0], profile[0]]

    if isinstance(radius, list) and radius[1] is None:
        radius = [radius[0], radius[0]]
    elif not isinstance(radius, list):
        radius = [radius, radius]
    for i, r in enumerate(radius):
        if r is None:
            radius[i] = {"fast": 1, None: 1, "lc": 2, "np": 3, "high": 4, "vn": 4}[profile[i]]

    if not (sigma[0][1] + sigma[0][2] + sigma[1][1] + sigma[1][2]):
        src = get_y(src)

    if src.format.color_family == vs.GRAY:
        chroma = False

    # guess the range based on either frame size (where unspec.) or from frame props
    if full_range is None:
        full_range = False if src.format.color_family in [vs.GRAY, vs.YUV] else True
        props = src.get_frame(0).props
        if "_ColorRange" in props:
            full_range = not props["_ColorRange"]

    if is_gray(src):
        matrix = None
    elif matrix is None:
        matrix = format.guess_matrix(src)

    def to_opp(v):
        v = format.set(
            v,
            formatstr="rgbs" if not is_gray(v) else "s",
            resizer=core.resize.Bicubic,
            filter_param_a=0,
            filter_param_b=0.5,
            matrix_in_s=matrix,
        )
        return core.bm3d.RGB2OPP(v, 1) if not is_gray(v) else v

    if CUDA[0] and pre is not None:
        raise print("BM3D: WARN --> BM3DCUDA doesn't accept a pre for the basic estimate, ignoring")

    clips = {k: to_opp(v) for k, v in dict(src=src, pre=pre, ref=ref).items() if v is not None}

    if all(c not in clips.keys() for c in ["pre", "ref"]):
        clips["pre"] = clips["src"]

    # region profiles
    # fmt: off
    CUDA_BASIC_PROFILES: Dict[Union[str, None], Dict[str, int]] = {
        "fast": dict(block_step=8, bm_range=9,  ps_range=4), # noqa
        "lc":   dict(block_step=6, bm_range=9,  ps_range=4), # noqa
        "np":   dict(block_step=4, bm_range=16, ps_range=5), # noqa
        "high": dict(block_step=2, bm_range=16, ps_range=7), # noqa
        "vn":   dict(block_step=4, bm_range=16, ps_range=5), # noqa
        None: {},
    }

    CUDA_FINAL_PROFILES: Dict[Union[str, None], Dict[str, int]] = {
        "fast": dict(block_step=7, bm_range=9,  ps_range=5), # noqa
        "lc":   dict(block_step=5, bm_range=9,  ps_range=5), # noqa
        "np":   dict(block_step=3, bm_range=16, ps_range=6), # noqa
        "high": dict(block_step=2, bm_range=16, ps_range=8), # noqa
        # original vn profile for final uses a block size of 11, where cuda only supports 8. vn used
        # step 11, and 11-4 = an overlap of 7, meaning the closest I can really get is a step of 1.
        # still probably isn't ideal, a larger block size would be far better for noisy content.
        "vn":   dict(block_step=1, bm_range=16, ps_range=6), # noqa
        None: {},
    }
    # fmt: on
    # endregion profiles

    if any(CUDA):
        if CUDA[1] and profile[1] == "vn":
            print(
                "BM3D: WARN --> BM3DCUDA does not directly support the vn profile for final"
                " estimate, Emulating nearest parameters.",
                file=sys.stderr,
            )
        cudaargs = [CUDA_BASIC_PROFILES[profile[0]], CUDA_FINAL_PROFILES[profile[1]]]
        for i in [0, 1]:
            if radius[i]:
                if profile[i] == "fast":
                    cudaargs[i].update(bm_range=7)
                elif profile[i] in ["np", "vn"]:
                    cudaargs[i].update(bm_range=12)
        for args in cudaargs:
            args.update(chroma=chroma, device_id=device_id, fast=fast)

    if not (skip_basic or "ref" in clips.keys()):
        if not CUDA[0]:
            basicargs = dict(
                input=clips["src"], ref=clips["pre"], profile=profile[0], sigma=sigma[0], matrix=100
            )
            if not radius[0]:
                basic = core.bm3d.Basic(**basicargs)
            else:
                basic = core.bm3d.VBasic(radius=radius[0], **basicargs).bm3d.VAggregate(
                    radius[0], 1
                )
        else:
            basic = core.bm3dcuda.BM3D(
                clips["src"], sigma=sigma[0], radius=radius[0], **cudaargs[0]
            )
            basic = core.bm3d.VAggregate(basic, radius[0], 1) if radius[0] else basic
    else:
        basic = clips["ref"] or clips["src"]

    final = basic
    for _ in range(refine):
        if not CUDA[1]:
            finalargs = dict(
                input=clips["src"], ref=final, profile=profile[1], sigma=sigma[1], matrix=100
            )
            if not radius[1]:
                final = core.bm3d.Final(**finalargs)
            else:
                final = core.bm3d.VFinal(**finalargs, radius=radius[1]).bm3d.VAggregate(
                    radius[1], 1
                )
        else:
            final = core.bm3dcuda.BM3D(
                clips["src"], ref=final, sigma=sigma[1], radius=radius[1], **cudaargs[1]
            )

            final = core.bm3d.VAggregate(final, radius[1], 1) if radius[1] else final

    out = core.bm3d.OPP2RGB(final, 1) if not is_gray(final) else final
    out = format.make_similar(
        out,
        src,
        resizer=core.resize.Bicubic,
        filter_param_a=0,
        filter_param_b=0.5,
        matrix_s=matrix,
    )
    if input_original.format.num_planes == 3:
        # we didn't upload chroma
        if src.format.color_family == vs.GRAY:
            out = join([out] + split(input_original)[1:], input_original.format.color_family)
        else:
            # bm3dcuda seems to set the luma to garbage if it isnt processed
            for i in [0, 1]:
                if CUDA[i] and not sigma[i][0] and (sigma[i][1] + sigma[i][2]):
                    out = join(
                        [get_y(input_original)] + split(out)[1:], input_original.format.color_family
                    )

    return out

def hybriddenoise(
    src: vs.VideoNode,
    sigma: int = 1,
    knlm: float = 0.5,
    radius: int = 1,
    profile: str = "np",
    refine: int = 1,
    CUDA: bool = True):

    """
    denoise luma with BM3D (CPU-based) and chroma with KNLMeansCL (GPU-based)
    sigma = luma denoise strength
    knl = chroma denoise strength. The algorithm is different, so this value is different from sigma
    BM3D's sigma default is 5, KNL's is 1.2, to give you an idea of the order of magnitude
    radius1 = temporal radius of luma denoising, 0 for purely spatial denoising
    """

    luma = vsutil.get_y(src)
    luma = BM3D(luma, sigma=sigma, radius=radius, profile=profile, refine=refine, CUDA=CUDA)
    chroma = core.knlm.KNLMeansCL(src, a=2, h=knlm, d=3, device_type='gpu', device_id=0, channels='UV')
    return core.std.ShufflePlanes([luma, chroma], planes=[0, 1, 2], colorfamily=vs.YUV)

def dfttest(clip, **dfttest_args):
    keys = dfttest_args.keys()
    if not any(param in keys for param in ["slocation", "ssx", "ssy", "sst"]):
        dfttest_args.update(slocation=eoe.freq._slocation)
    if "sst" in keys and ("tbsize" not in keys or keys["tbsize"] < 2):
        raise ValueError("tbsize should be > 1 when using sst")
    kwargs = {**eoe.freq._dfttest_args, **dfttest_args}
    return core.dfttest.DFTTest(clip, **kwargs)


def merge_frequency(low, hi, **dfttest_args):
    hif = core.std.MakeDiff(hi, dfttest(hi, **dfttest_args))
    clip = core.std.MergeDiff(dfttest(low, **dfttest_args), hif)
    return clip

def dft_ccd(src: vs.VideoNode, sloc: list[float] = [0, 0.2, 0.5, 0.3, 0.75, 6, 1, 10], thr: int = 2, tr: int = 3, csharp: bool = False):
    """
    Convenience function for dfttest on static luma and ccd for temporal/static chroma denoising
    Stock settings are for general use cases. sloc requires manual tuning based on the source.

    :param src:         Input to denoise
    :param sloc:        [Frequency, Denoising Strength] Same as dfttest documentation (Uses a specific denoising strength at a given frequency.)
    :param thr:         Strength of CCD on chroma.
    :param tr:          Temporal Radius used for CCD.
    :param csharp:      Apply contrasharpening after denoising.

    :return:            Denoised clip
    """
    dft = core.dfttest.DFTTest(src, planes=[0], tbsize=1, slocation=sloc)
    out = ccd(dft, thr=thr, tr=tr, ref_points=CCDPoints.ALL, matrix=1)
    if csharp != False:
        out = contrasharpening(out, src, mode = 3 if csharp == True else csharp)
    return out
