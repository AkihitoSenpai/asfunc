from setuptools import setup
from distutils.util import convert_path

# stolen from https://github.com/Irrational-Encoding-Wizardry/vsutil/blob/master/setup.py
meta = {}
exec(open(convert_path("ASFunc/_metadata.py")).read(), meta)


setup(
    name="ASFunc",
    version=meta["__version__"],
    packages=["ASFunc"],
    author="Akihito",
    description="Useful function wrappers for AkihitoSubs",
    url="https://gitlab.com/AkihitoSenpai/asfunc.git",
    install_requires=["vapoursynth", "numpy"],
    python_requires=">=3.8",
)
