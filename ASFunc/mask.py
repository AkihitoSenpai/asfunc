from functools import partial
from vsutil import *
import vapoursynth as vs
import mvsfunc as mvf
import fvsfunc as fvf
import kagefunc as kgf
import lvsfunc as lvf
from debandshit import dumb3kdb
from typing import Any, Dict, List, Optional, Tuple, Union, Callable
import vsutil
from vsutil.clips import depth, get_y, join, plane, split
from vsutil.func import iterate
from vsutil.clips import get_y

core = vs.core


def retinex_edgemask(src: vs.VideoNode, sigma=1, opencl: bool = False, device: int = -1) -> vs.VideoNode:
    """
    Same as kagefuncs retinex_edgemask with the addition of
    opencl support.

    :param src:      Input a clip to create an edgemask
    :param sigma:    Strength of the edgemask, lower protects more detail. (Default: 1)
    :param opencl:   Enable opencl support (Default: False)
    :param device: Select the opencl device to use.

    :return:         Edgemask
    """

    luma = kgf.get_y(src)
    max_value = 1 if src.format.sample_type == vs.FLOAT else (1 << kgf.get_depth(src)) - 1
    ret = core.retinex.MSRCP(luma, sigma=[50, 200, 350], upper_thr=0.005)
    tcanny = ret.tcanny.TCanny(mode=1, sigma=sigma).std.Minimum(coordinates=[1, 0, 1, 0, 0, 1, 0, 1])
    return core.std.Expr([kgf.kirsch(luma), tcanny], f'x y + {max_value} min')

def _get_bits(clip: vs.VideoNode, expected_depth: int = 16) -> Tuple[int, vs.VideoNode]:
    from vsutil import get_depth

    bits = get_depth(clip)
    return bits, depth(clip, expected_depth) if bits != expected_depth else clip

def detail_mask(clip: vs.VideoNode,
                sigma: float = 1.0, rxsigma: List[int] = [50, 200, 350],
                pf_sigma: Optional[float] = 1.0,
                brz_a: Tuple[int, int] = (2500, 8000),
                brz_b: Tuple[int, int] = (4000, 12000),
                brz_c: int = 12000,
                rg_mode: int = 17,
                ) -> vs.VideoNode:

    """
    Generate a detail mask for denoising.

    :param clip:   Input clip. 
    :param sigma:  Decides how strong the gaussian blur will be.
    :param rxsigma:  Strength of MSRCP.
    :param pf_sigma:  Strength of the prefilter gaussian blur.
    :param brz_a:  Binarize the blurred retinex clip. 
    :param brz_b:  Binarize the inflated retinex edgemask.
    :param brz_c:  Binarize the retinex edgemask clip.
    :param rg_mode:  Mode to use for the RemoveGrain filter.



    :return:       Detail mask (Bit Depth: 16)
    """

    bits, clip = _get_bits(clip)
    clip_y = get_y(clip)

    pf = core.bilateral.Gaussian(clip_y, sigma=pf_sigma) if pf_sigma else clip_y
    ret = core.retinex.MSRCP(pf, sigma=rxsigma, upper_thr=0.005)
    blur_ret = core.bilateral.Gaussian(ret, sigma=sigma)
    blur_ret_diff = core.std.Expr([blur_ret, ret], "x y -")
    blur_ret_dfl = core.std.Deflate(blur_ret_diff)
    blur_ret_ifl = iterate(blur_ret_dfl, core.std.Inflate, 4)
    blur_ret_brz = core.std.Binarize(blur_ret_ifl, brz_a[0])
    blur_ret_brz = core.morpho.Close(blur_ret_brz, size=8)

    retinex_mask = retinex_edgemask(clip_y, sigma=0, opencl=True, device=0).std.Binarize(brz_c)
    retinex_ifl = retinex_mask.std.Deflate().std.Inflate()
    retinex_brz = core.std.Binarize(retinex_ifl, brz_b[1])
    retinex_morpo = core.morpho.Close(retinex_brz, size=4)

    merged = core.std.Expr([blur_ret_brz, retinex_morpo], "x y +")
    rm_grain = core.rgvs.RemoveGrain(merged, rg_mode)
    return rm_grain if bits == 16 else depth(rm_grain, bits)

def debandmask(src: vs.VideoNode, 
               maximum: int = 1,
               inflate: int = 1,
               brz_min: int = 2000,
               brz_max: int = 9892):

    luma = get_y(src)
    ret = core.std.Expr(luma, 'x 65535 / sqrt 65535 *')
    sx = ret.std.Convolution([-1, -2, -1, 0, 0, 0, 1, 2, 1], saturate=False)
    sy = ret.std.Convolution([-1, 0, 1, -2, 0, 2, -1, 0, 1], saturate=False)
    merge = core.std.Expr([sx, sy], 'x y max')
    thick = vsutil.iterate(merge, core.std.Maximum, maximum)
    inf = vsutil.iterate(thick, core.std.Inflate, inflate)
    
    return inf.std.Binarize((brz_min,brz_max)[0])

    #if denoise == True:
    #return core.knlm.KNLMeansCL(brz, a=30, h=30, d=1, device_type='gpu', device_id=0)
    #dmask = core.std.Convolution(rg, matrix=[1,1,1,1,1,1,1,1,1])

def minmax_clamp(src: vs.VideoNode, aa: vs.VideoNode, radius: int = 1):

    # Stolen from EoEFunc (Thanks <3)

    """
    Clamp aa to surrounding source clip pixel minimum/maximums.
    Quick and easy way to reduce excessive haloing caused by an aa.
    Not line sensitive - won't remove halos, will only clamp overbrightened pixels.
    :param src: Source clip.
    :param aa: AA'd clip.
    :radius: Radius to search for clamping (Default: 1)
    :return: Clip with clamped anti-aliasing
    """
    from vsutil import iterate

    minimum = iterate(src, core.std.Minimum, radius)
    maximum = iterate(src, core.std.Maximum, radius)

    return core.std.Expr([aa, minimum, maximum], "x y max z min")

def fast_sobel(src: vs.VideoNode) -> vs.VideoNode:
    luma = get_y(src)
    sx = luma.std.Convolution([-1, -2, -1, 0, 0, 0, 1, 2, 1], saturate=False)
    sy = luma.std.Convolution([-1, 0, 1, -2, 0, 2, -1, 0, 1], saturate=False)
    return core.std.Expr([sx, sy], 'x y max')

def detailmask(src: vs.VideoNode, sigma=1, lines_brz=0.02):
    """
    Create a detailmask to protect detail loss against denoising
    and banding.

    :param src:      Input a clip to create an edgemask
    :param sigma:    Strength of the edgemask, lower protects more detail. (Default: 1)
    :param lines_brz: Binarizing for the prewitt mask.

    :return:         Detailmask
    """

    y = get_y(src)
    ret = core.std.Expr(y, 'x 65535 / sqrt 65535 *')
    return lvf.mask.detail_mask_neo(ret, sigma=sigma, lines_brz=lines_brz).rgvs.RemoveGrain(4)
