import vapoursynth as vs

if vs.__api_version__.api_major < 4:
    vs.VideoFormat = vs.Format
else:
    vs.YCOCG = None

from . import format, denoise, scale, mask, deband

from .format import *
from .denoise import *
from .scale import *
from .mask import *
from .deband import *

try:
    from ._metadata import __version__, __author__
except ImportError:
    __version__ = __author__ = "Unknown"

# submodule aliases
fmt = format
dn = denoise

